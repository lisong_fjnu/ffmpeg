package com.ls.ffmpeg.foundation;

import org.bytedeco.javacv.*;
import org.bytedeco.javacv.Frame;
import org.bytedeco.opencv.opencv_core.IplImage;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 *
 * @ClassName: VideoWatermarkUtil
 * @Description:
 * @Author: LS
 * @Date: 2023/8/3 10:38
 */
@Component
public class VideoWatermarkUtil {

    public String addVideoWatermark(String filePath,String outFileName,String drawString,int size,int x,int y) throws FrameGrabber.Exception, FFmpegFrameRecorder.Exception {
        //String filePath="C:\\112.mp4";
        File file=new File(filePath);
        //抓取视频资源
        FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(file);
        //
        Frame frame = null;
        FFmpegFrameRecorder recorder = null;
        //String fileName = null;

            frameGrabber.start();
            Random random=new Random();
            //fileName = file.getAbsolutePath() + random.nextInt(100)+".mp4";
            System.out.println("文件名-->>"+outFileName);
            recorder = new FFmpegFrameRecorder(outFileName, frameGrabber.getImageWidth(), frameGrabber.getImageHeight(), frameGrabber.getAudioChannels());
//            recorder.setFormat("mp4");
            recorder.setSampleRate(frameGrabber.getSampleRate());
            recorder.setFrameRate(frameGrabber.getFrameRate());
            recorder.setTimestamp(frameGrabber.getTimestamp());
            recorder.setVideoBitrate(frameGrabber.getVideoBitrate());
            recorder.setVideoCodec(frameGrabber.getVideoCodec());

            recorder.start();
            int index=0;
            while (true){
                frame=frameGrabber.grabFrame();
                if(frame==null){
                    System.out.println("视频处理完成");
                    break;
                }
                //判断音频
                //System.out.println("音频=="+(frame.samples ==null)+"视频=="+ (frame.image==null));
                //判断图片帧
                if(frame.image !=null){
                    IplImage iplImage = Java2DFrameUtils.toIplImage(frame);
                    BufferedImage buffImg=Java2DFrameUtils.toBufferedImage(iplImage);
                    Graphics2D graphics = buffImg.createGraphics();
                    graphics.setColor(Color.BLUE);//水印文字颜色
                    graphics.setFont(new Font("微软雅黑", Font.BOLD, size));//字体 是否加粗 大小
                    //graphics.drawString("20230803102240",(iplImage.width()/2)-100,iplImage.height()/2);
                    graphics.drawString(drawString,x,y);//水印文字内容、相对位置
                    System.out.println(iplImage.width());
                    System.out.println(iplImage.height());
                    graphics.dispose();
                    Frame newFrame = Java2DFrameUtils.toFrame(buffImg);
                    recorder.record(newFrame);
                }
                //设置音频
                if(frame.samples !=null){
                    recorder.recordSamples(frame.sampleRate,frame.audioChannels,frame.samples);
                }
                //System.out.println("帧值="+index);
                index ++;
            }
            recorder.stop();
            recorder.release();
            frameGrabber.stop();

        return "success";
    }

}
