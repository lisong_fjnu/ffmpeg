package com.ls.ffmpeg.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Description swagger的配置类
 * @Author LS
 * @Date 2023/8/2 16:18
 * @Version 1.0
 * http://localhost:8087/swagger-ui.html
 **/
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("swagger服务")
                .description("swagger服务-api接口")
                .termsOfServiceUrl("http://www.localhost:8087")
                .version("1.0.0")
                .build();
    }

    @Bean
    public Docket createLayerApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
                .groupName("1.录屏服务")
                .select()
                // 方法需要有ApiOperation注解才能生存接口文档
                .apis(RequestHandlerSelectors.basePackage("com.ls.ffmpeg.controller"))
                // 路径使用any风格
                .paths(PathSelectors.any())
                .build();
    }
}