package com.ls.ffmpeg.controller;

/**
 * Created with IntelliJ IDEA.
 *
 * @ClassName: VideoWatermark
 * @Description:
 * @Author: LS
 * @Date: 2023/8/3 10:07
 */
import java.io.File;
import java.io.IOException;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameUtils;
import org.bytedeco.opencv.opencv_core.IplImage;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class VideoWatermark {
    public static void main(String[] args) {
        String filePath="C:\\112.mp4";
        File file=new File(filePath);
        //抓取视频资源
        FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(file);
        //
        Frame frame = null;
        FFmpegFrameRecorder recorder = null;
        String fileName = null;
        try{
            frameGrabber.start();
            Random random=new Random();
            fileName = file.getAbsolutePath() + random.nextInt(100)+".mp4";
            System.out.println("文件名-->>"+fileName);
            recorder = new FFmpegFrameRecorder(fileName, frameGrabber.getImageWidth(), frameGrabber.getImageHeight(), frameGrabber.getAudioChannels());
//            recorder.setFormat("mp4");
            recorder.setSampleRate(frameGrabber.getSampleRate());
            recorder.setFrameRate(frameGrabber.getFrameRate());
            recorder.setTimestamp(frameGrabber.getTimestamp());
            recorder.setVideoBitrate(frameGrabber.getVideoBitrate());
            recorder.setVideoCodec(frameGrabber.getVideoCodec());

            recorder.start();
            int index=0;
            while (true){
                frame=frameGrabber.grabFrame();
                if(frame==null){
                    System.out.println("视频处理完成");
                    break;
                }
                //判断音频
                //System.out.println("音频=="+(frame.samples ==null)+"视频=="+ (frame.image==null));
                //判断图片帧
                if(frame.image !=null){
                    IplImage iplImage = Java2DFrameUtils.toIplImage(frame);
                    BufferedImage buffImg=Java2DFrameUtils.toBufferedImage(iplImage);
                    Graphics2D graphics = buffImg.createGraphics();
                    graphics.setColor(Color.white);
                    graphics.setFont(new Font("微软雅黑", Font.BOLD, 60));
                    //graphics.drawString("20230803102240",(iplImage.width()/2)-100,iplImage.height()/2);
                    graphics.drawString("20230803102240",100,100);
                    System.out.println(iplImage.width());
                    System.out.println(iplImage.height());
                    graphics.dispose();
                    Frame newFrame = Java2DFrameUtils.toFrame(buffImg);
                    recorder.record(newFrame);
                }
                //设置音频
                if(frame.samples !=null){
                    recorder.recordSamples(frame.sampleRate,frame.audioChannels,frame.samples);
                }
                //System.out.println("帧值="+index);
                index ++;
            }
            recorder.stop();
            recorder.release();
            frameGrabber.stop();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
